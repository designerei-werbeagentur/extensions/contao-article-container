<?php

declare(strict_types=1);

// fields
$GLOBALS['TL_DCA']['tl_article']['fields']['containerMaxWidth'] = array
(
    'exclude'                 => true,
    'inputType'               => 'select',
    'eval'                    => array('tl_class'=>'w50', 'includeBlankOption'=>true),
    'sql'                     => "varchar(32) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_article']['fields']['containerClass'] = array
(
    'exclude'                 => true,
    'inputType'               => 'text',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "varchar(255) NOT NULL default ''"
);

$GLOBALS['TL_DCA']['tl_article']['fields']['containerCenter'] = array
(
    'exclude'                 => true,
    'inputType'               => 'checkbox',
    'default'                 => '1',
    'eval'                    => array('tl_class'=>'w50'),
    'sql'                     => "char(1) NOT NULL default '1'"
);
