# Contao-Article-Container

## Function & usage

This extension for Contao Open Source CMS adds an container element to the mod_article template and gives you the option to adjust the maximum width of the container. You can select between multiple maximum width options for the container element. Before use, the options must be configured within the `config.yml`. Also you can center and add one or multiple classes to the container element.

Note: Within the template `mod_article_container` there is an `inside` container as wrapper element.If you also use the extension designerei/contao-article-layout you must merge both templates (`mod_article_background` and `mod_article_layout`) into one.

## Configuration

The default configuration of the options for maximum width is empty. You have to define it by adjusting the `config.yml` file.

```yml
contao_article_container:
  maximum_width:
    - foo
    - bar
```
