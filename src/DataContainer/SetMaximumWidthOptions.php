<?php

declare(strict_types=1);

namespace designerei\ContaoArticleContainerBundle\DataContainer;

use Contao\CoreBundle\ServiceAnnotation\Callback;

/**
 * @Callback(table="tl_article", target="fields.containerMaxWidth.options")
 */
class SetMaximumWidthOptions
{
    private array $config;

    public function __construct(array $config)
    {
        $this->maxWidthOptions = $config;
    }

    public function __invoke(): array {
        return $this->maxWidthOptions;
    }
}
