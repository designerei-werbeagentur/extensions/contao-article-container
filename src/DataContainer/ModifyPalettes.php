<?php

declare(strict_types=1);

namespace designerei\ContaoArticleContainerBundle\DataContainer;

use Contao\CoreBundle\DataContainer\PaletteManipulator;
use Contao\CoreBundle\ServiceAnnotation\Callback;
use Contao\DataContainer;

class ModifyPalettes
{
    /**
     * @Callback(table="tl_article", target="config.onload")
     */
    public function __invoke(DataContainer $dc): void
    {
        PaletteManipulator::create()
          ->addLegend('article_legend', 'expert_legend', PaletteManipulator::POSITION_BEFORE)
          ->addField([
              'containerMaxWidth',
              'containerClass',
              'containerCenter'
              ],
              'article_legend',
              PaletteManipulator::POSITION_APPEND
          )
          ->applyToPalette('default', 'tl_article')
        ;
    }
}
