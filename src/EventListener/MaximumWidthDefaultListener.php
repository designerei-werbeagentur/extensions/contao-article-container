<?php

declare(strict_types=1);

namespace designerei\ContaoArticleContainerBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Callback;

/**
 * @Callback(table="tl_article", target="fields.maxWidth.load")
 */
class MaximumWidthDefaultListener
{
    private string $config;

    public function __construct(string $config)
     {
       $this->maxWidthDefault = $config;
     }

     public function __invoke(): string {
       return $this->maxWidthDefault;
     }
}
