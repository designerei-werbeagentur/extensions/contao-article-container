<?php

declare(strict_types=1);

namespace designerei\ContaoArticleContainerBundle\EventListener;

use Contao\CoreBundle\ServiceAnnotation\Callback;

/**
 * @Callback(table="tl_article", target="fields.maxWidth.options")
 */
class MaximumWidthOptionsListener
{
    private array $config;

    public function __construct(array $config)
     {
       $this->maxWidthOptions = $config;
     }

     public function __invoke(): array {
       return $this->maxWidthOptions;
     }
}
