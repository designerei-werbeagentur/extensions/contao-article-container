<?php

declare(strict_types=1);

namespace designerei\ContaoArticleContainerBundle\DependencyInjection;

use designerei\ContaoArticleContainerBundle\DataContainer\SetMaximumWidthOptions;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\DependencyInjection\Definition;


class ContaoArticleContainerExtension extends Extension
{
    public function getAlias(): string
    {
        return 'contao_article_container';
    }

    public function load(array $configs, ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__ . '/../../config'));
        $loader->load('services.yml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $optionsDefinition = $container->getDefinition(SetMaximumWidthOptions::class);
        $optionsDefinition->setArgument(0, $config['maximum_width']);
    }
}
