<?php

declare(strict_types=1);

namespace designerei\ContaoArticleContainerBundle;

use designerei\ContaoArticleContainerBundle\DependencyInjection\ContaoArticleContainerExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class ContaoArticleContainerBundle extends Bundle
{
    public function getPath(): string
    {
        return \dirname(__DIR__);
    }

    public function getContainerExtension(): ContaoArticleContainerExtension
    {
        return new ContaoArticleContainerExtension();
    }
}
